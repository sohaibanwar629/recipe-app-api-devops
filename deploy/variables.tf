variable "prefix" {
  default = "raad"
}

# ... existing file ...

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "sohaibanwar629@gmail.com"
}
